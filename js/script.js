'use strict';
/**
 * Module that Manages clients
 */
var clientModule = (function() {

	const domStrings = {
		name: '#name',
		firstName: '#firstName',
		file: '#file',
		add: '#add',
		clients: '#clients tbody',
		search: '#search'
	};

	// Array that stores the clients added
	var clients = [];

	// template that returns a single client
	const template = (({name, firstName, photo}) => {
		return `<tr>
					<td><img src="${photo}" width="50px" height="50px"/></td>
					<td>${name}</td>
					<td>${firstName}</td>
				</tr>`;
	});

	/**
	 * Funtion that filters text search
	 */
	function filterClients () {
		let search = document.querySelector(domStrings.search).value;

		return clients.filter((client) => {

			let clientName = client.name.toLocaleLowerCase(),
				clientFirstName = client.firstName.toLocaleLowerCase();

			return clientName.indexOf(search) !== -1 || clientFirstName.indexOf(search) !== -1;
		});	
	}

	/**
	 * Render the filtered clients to the DOM
	 */
	function renderClients () {
		let filteredClients = filterClients({});
		let html = filteredClients.map(template).join('');

		document.querySelector(domStrings.clients).innerHTML = html;
	}

	/**
	 * [addClient - Add client to data structure and DOM]
	 */
	function addClient () {

		let fileValue = document.querySelector(domStrings.file).value.split('\\');

		// Get client values from DOM
		const client = {
			name: document.querySelector(domStrings.name).value,
			firstName: document.querySelector(domStrings.firstName).value,
			photo: 'img/' + fileValue[2]
		}

		// Store client to data structure
		clients.push(client);

		// Add client to DOM
		addClientDOM(client);

		// Clear inputs from text
		clearInputs();
	}

	function addClientDOM (client) {
		let element = template(client);
		document.querySelector(domStrings.clients).insertAdjacentHTML('beforeend', element);
	}

	function clearInputs () {
		document.querySelector(domStrings.name).value = '';
		document.querySelector(domStrings.firstName).value = '';
		document.querySelector(domStrings.file).value = '';
	}

	function bindEvents () {
		document.querySelector(domStrings.add).addEventListener('click', addClient);
		document.querySelector(domStrings.search).addEventListener('keyup', renderClients);
	}

	function init (mock) {

		// 1. Add the predefined list of clients and store it
		clients = [...mock];

		// 2. Render Clients
		renderClients(clients);
		
		// 3. Bind Events
		bindEvents();
	}

	return {
		init: init
	};

}());

clientModule.init(list);